Put Vagrantfile into a vagrant directory

Run `vagrant up`

Run `vagrant ssh manager`

Run `sudo docker service create --replicas 3 --name nginx --publish 80:80 nginx`

Run outside the VM `python smoke.py endpoints.yml --proto=http`